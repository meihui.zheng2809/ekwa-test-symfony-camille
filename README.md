# ekwa-test-symfony-camille

This project is aimed to check validity of a promo code and find its available offers.


**configuration**

Install Console component :

> composer require symfony/console

Install PHPUnit :

> composer require --dev phpunit/phpunit symfony/test-pack

Install Filesystem Component :

> composer require symfony/filesystem


**Command** 

> bin/console promo-code:validate MON_CODE_PROMO_A_TESTER



**API**

Data for promo code and offers couled be catched by APIs:

- [promo codes list ](https://601025826c21e10017050013.mockapi.io/ekwatest/promoCodeList)

- [offers list](https://601025826c21e10017050013.mockapi.io/ekwatest/offerList)



**Result** 

Result file (json) is saved in `public/files`
