<?php


namespace App\Tests;


use App\Command\PromoCodeCommand;
use PHPUnit\Framework\TestCase;

class PromoCodeCommandTest extends TestCase
{
    public function testGetPromoAndOffer ()
    {
        $promoCodeValidate = new PromoCodeCommand();
        $result = $promoCodeValidate->getPromoAndOffer(
            'https://601025826c21e10017050013.mockapi.io/ekwatest/promoCodeList');
        $expected = [
            0 =>  [
                "code" => "EKWA_WELCOME",
                "discountValue" => 2,
                "endDate" => "2019-10-04",
            ],
            1 =>  [
                "code" => "ELEC_N_WOOD",
                "discountValue" => 1.5,
                "endDate" => "2022-06-20",
            ],
            2 =>  [
                "code" => "ALL_2000",
                "discountValue" => 2.75,
                "endDate" => "2023-03-05",
            ],
            3 =>  [
                "code" => "GAZZZZZZZZY",
                "discountValue" => 2.25,
                "endDate" => "2018-08-02",
            ],
            4 =>  [
                "code" => "ELEC_IS_THE_NEW_GAS",
                "discountValue" => 3.5,
                "endDate" => "2022-04-13",
            ],
            5 =>  [
                "code" => "BUZZ",
                "discountValue" => 2.75,
                "endDate" => "2022-02-02",
            ],
            6 =>  [
                "code" => "WOODY",
                "discountValue" => 1.75,
                "endDate" => "2022-05-29",
            ],
            7 =>  [
                "code" => "WOODY_WOODPECKER",
                "discountValue" => 1.15,
                "endDate" => "2017-04-07",
            ],
        ];
        $this->assertEquals($expected, $result);
    }

    public function testCheckPromoCodeWithExistedCode()
    {
        $promoCodeValidate = new PromoCodeCommand();
        $codeList = [
            0 =>  [
                "code" => "EKWA_WELCOME",
                "discountValue" => 2,
                "endDate" => "2019-10-04",
            ],
            1 =>  [
                "code" => "ELEC_N_WOOD",
                "discountValue" => 1.5,
                "endDate" => "2022-06-20",
            ],
            2 =>  [
                "code" => "ALL_2000",
                "discountValue" => 2.75,
                "endDate" => "2023-03-05",
            ],
            3 =>  [
                "code" => "GAZZZZZZZZY",
                "discountValue" => 2.25,
                "endDate" => "2018-08-02",
            ],
        ];
        $result = $promoCodeValidate->checkPromoCode('ELEC_N_WOOD', $codeList);

        $expected = [
            "promoCode" => "ELEC_N_WOOD",
            "endDate" => "2022-06-20",
            "discountValue" => 1.5,
            "compatibleOfferList" => [],
        ];

        $this->assertEquals($expected, $result);
    }

    public function testCheckPromoCodeWithNotExistedCode()
    {
        $promoCodeValidate = new PromoCodeCommand();
        $codeList = [
            0 =>  [
                "code" => "EKWA_WELCOME",
                "discountValue" => 2,
                "endDate" => "2019-10-04",
            ],
            1 =>  [
                "code" => "ALL_2000",
                "discountValue" => 2.75,
                "endDate" => "2023-03-05",
            ],
            3 =>  [
                "code" => "GAZZZZZZZZY",
                "discountValue" => 2.25,
                "endDate" => "2018-08-02",
            ],
        ];
        $result = $promoCodeValidate->checkPromoCode('ELEC_N_WOODKKK', $codeList);

        $expected = [];

        $this->assertEquals($expected, $result);
    }

    public function testCheckPromoCodeWithExpiredCode()
    {
        $promoCodeValidate = new PromoCodeCommand();
        $codeList = [
            0 =>  [
                "code" => "EKWA_WELCOME",
                "discountValue" => 2,
                "endDate" => "2019-10-04",
            ],
            1 =>  [
                "code" => "ALL_2000",
                "discountValue" => 2.75,
                "endDate" => "2023-03-05",
            ],
            3 =>  [
                "code" => "GAZZZZZZZZY",
                "discountValue" => 2.25,
                "endDate" => "2018-08-02",
            ],
        ];
        $result = $promoCodeValidate->checkPromoCode('EKWA_WELCOME', $codeList);

        $expected = [];

        $this->assertEquals($expected, $result);
    }


    public function testGetCompatibleOffersExisted()
    {
        $promoCodeValidate = new PromoCodeCommand();
        $offerList = [
            0 =>  [
                "offerType" => "ELECTRICITY",
                "offerName" => "EKWAE3000",
                "offerDescription" => "Pile l'offre qu'il vous faut",
                "validPromoCodeList" =>  [
                    0 => "EKWA_WELCOME",
                    1 => "ELEC_IS_THE_NEW_GAS",
                    2 => "BUZZ",
                    3 => "ELEC_N_WOOD",
                ],
            ],
            1 =>  [
                "offerType" => "WOOD",
                "offerName" => "EKWAW2000",
                "offerDescription" => "Une offre du envoie du bois",
                "validPromoCodeList" =>  [
                    0 => "EKWA_WELCOME",
                    1 => "ALL_2000",
                    2 => "WOODY",
                    3 => "WOODY_WOODPECKER",
                ],
            ],
            2 =>  [
                "offerType" => "WOOD",
                "offerName" => "EKWAW3000",
                "offerDescription" => "Une offre souscrite = un arbre planté",
                "validPromoCodeList" =>  [
                    0 => "EKWA_WELCOME",
                    1 => "WOODY",
                    2 => "WOODY_WOODPECKER",
                    3 => "ELEC_N_WOOD",
                ],
            ],
        ];
        $result = $promoCodeValidate->getCompatibleOffers('ELEC_N_WOOD', $offerList);

        $expected = [
            0 => [
                "offerType" => "ELECTRICITY",
                "offerName" => "EKWAE3000",
                "offerDescription" => "Pile l'offre qu'il vous faut",
                "validPromoCodeList" => [
                    0 => "EKWA_WELCOME",
                    1 => "ELEC_IS_THE_NEW_GAS",
                    2 => "BUZZ",
                    3 => "ELEC_N_WOOD",
                ],
            ],
            1 => [
                "offerType" => "WOOD",
                "offerName" => "EKWAW3000",
                "offerDescription" => "Une offre souscrite = un arbre planté",
                "validPromoCodeList" => [
                    0 => "EKWA_WELCOME",
                    1 => "WOODY",
                    2 => "WOODY_WOODPECKER",
                    3 => "ELEC_N_WOOD",
                ],
            ],
        ];

        $this->assertEquals($expected, $result);
    }

    public function testGetCompatibleOffersNotExisted()
    {
        $promoCodeValidate = new PromoCodeCommand();
        $offerList = [
            0 =>  [
                "offerType" => "ELECTRICITY",
                "offerName" => "EKWAE3000",
                "offerDescription" => "Pile l'offre qu'il vous faut",
                "validPromoCodeList" =>  [
                    0 => "EKWA_WELCOME",
                    1 => "ELEC_IS_THE_NEW_GAS",
                    2 => "BUZZ",
                    3 => "ELEC_N_WOOD",
                ],
            ],
            1 =>  [
                "offerType" => "WOOD",
                "offerName" => "EKWAW2000",
                "offerDescription" => "Une offre du envoie du bois",
                "validPromoCodeList" =>  [
                    0 => "EKWA_WELCOME",
                    1 => "ALL_2000",
                    2 => "WOODY",
                    3 => "WOODY_WOODPECKER",
                ],
            ],
            2 =>  [
                "offerType" => "WOOD",
                "offerName" => "EKWAW3000",
                "offerDescription" => "Une offre souscrite = un arbre planté",
                "validPromoCodeList" =>  [
                    0 => "EKWA_WELCOME",
                    1 => "WOODY",
                    2 => "WOODY_WOODPECKER",
                    3 => "ELEC_N_WOOD",
                ],
            ],
        ];
        $result = $promoCodeValidate->getCompatibleOffers('ELEC_N_WOOD__2', $offerList);

        $expected = [];

        $this->assertEquals($expected, $result);
    }
}