<?php


namespace App\Command;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

class PromoCodeCommand extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'promo-code:validate';//Command for promo code validation

    /**
     * @var bool|resource
     */
    private $outputFile;

    public function __construct(string $projectDir = null)
    {
        $this->projectDir = $projectDir;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Promo Code Validation')
             ->addArgument('promoCode', InputArgument::REQUIRED, 'Promo Code');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Searching for Promo Code...');
        $inputPromoCode   = $input->getArgument('promoCode');

        //Get promo code and check if it's promo code list and not expired :
        $promoCodeList = $this->getPromoAndOffer(
            'https://601025826c21e10017050013.mockapi.io/ekwatest/promoCodeList'
        );
        //dd($promoCodeList);
        if (!$promoCodeList) {
            $output->writeln('Promo Code List not found !');
            return Command::FAILURE;
        }

        $promoCodeData = $this->checkPromoCode($inputPromoCode, $promoCodeList);
        if (0 == count($promoCodeData)) {
            $output->writeln('Promos code not valid !');
            return Command::FAILURE;
        }

        //Get offer compatible with promo code :
        $offerList = $this->getPromoAndOffer(
            'https://601025826c21e10017050013.mockapi.io/ekwatest/offerList'
        );
        if (!$offerList) {
            $output->writeln('Offer List not found !');
            return Command::FAILURE;
        }

        $compatibleOffer =  $this->getCompatibleOffers($inputPromoCode, $offerList);
        if (0 == count($compatibleOffer)) {
            $output->writeln('There\'s not any offer compatible with promos code !');
            return Command::FAILURE;
        }

        $this->createFile($promoCodeData, $compatibleOffer);
        $output->writeln($this->getSuccessMessage($inputPromoCode, $compatibleOffer));

        return Command::SUCCESS;
    }

    /**
     * @param string $url
     * @return array|false|mixed
     */
    public function getPromoAndOffer(string $url)
    {
        $data = json_decode(file_get_contents($url), true);

        if (!is_array($data)) {
            return false;
        }
        return $data;
    }

    /**
     * @param string $promoCode
     * @param array $codeList
     * @return array
     */
    public function checkPromoCode(string $promoCode, array $codeList)
    {
        $today = new \DateTime('now');
        $promoCodeData       = [];
        foreach ($codeList as $promoCodeItem) {
            if ($promoCode === $promoCodeItem['code']
                && $promoCodeItem['endDate'] > $today->format('Y-m-j')) {
                $promoCodeData['promoCode']           = $promoCodeItem['code'];
                $promoCodeData['endDate']             = $promoCodeItem['endDate'];
                $promoCodeData['discountValue']       = $promoCodeItem['discountValue'];
                $promoCodeData['compatibleOfferList'] = [];
                break;
            }
        }
        return $promoCodeData;
    }

    /**
     * @param string $promoCode
     * @param array $offerList
     * @return array
     */
    public function getCompatibleOffers(string $promoCode, array $offerList)
    {
        $offers = [];
        foreach ($offerList as $offerItem) {
            if (in_array($promoCode, $offerItem['validPromoCodeList'])) {
                $offers[] = $offerItem;
            }
        }
        return $offers;
    }

    /**
     * @param array $promoCodeData
     * @param array $compatibleOffer
     * @return void
     */
    protected function createFile(array $promoCodeData, array $compatibleOffer)
    {
        $filesystem = new Filesystem();
        if (!$filesystem->exists($this->projectDir . '/public/files')) {
            $filesystem->mkdir($this->projectDir . '/public/files', 0700);
        }

        foreach ($compatibleOffer as $offer) {
            $promoCodeData['compatibleOfferList'][] = [
                'name' => $offer['offerName'],
                'type' => $offer['offerType'],
            ];
        }

        $this->outputFile = fopen(
            $this->projectDir . '/public/files/compatibleOffers_'
            . $promoCodeData['promoCode'] . '.json',
            'w');
        fwrite($this->outputFile, json_encode($promoCodeData));
        fclose($this->outputFile);
    }

    /**
     * @param string $inputPromoCode
     * @param array $compatibleOffer
     * @return array
     */
    protected function getSuccessMessage(string $inputPromoCode, array $compatibleOffer)
    {
        $message   = [];
        $message[] = count($compatibleOffer) . ' offer(s) compatible with promos code -> '
            . $inputPromoCode;
        $message[] = '=========';
        foreach ($compatibleOffer as $offer) {
            $message[] = '- ' . $offer['offerDescription'];
        }
        $message[] = 'More information in generated file.';
        return $message;
    }

}